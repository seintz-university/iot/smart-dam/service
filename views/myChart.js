// timestamp, X axis
const ts = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
];

// level, Y axis
const level = [0, 10, 5, 2, 20, 30, 45];

const data = {
    labels: ts,
    datasets: [{
        label: 'Level',
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: level,
    }]
};

const config = {
    type: 'line',
    data: data,
    options: {}
};

const myChart = new Chart(
    document.getElementById('chartID'),
    config
);