// https://medium.com/swlh/building-a-simple-web-application-with-node-express-mongodb-dcd53231e83c

require('dotenv').config();
const mongoose = require('mongoose');
const URL = String(process.env.DB_URL);
mongoose.connect(URL, (err) => { console.log("Connection successful: " + err) });

const levelSchema = mongoose.Schema({
    timestamp: { type: String, unique: true, required: true },
    level: { type: String, required: true }
});

const Level = mongoose.model('Level', levelSchema);
module.exports = Level;