var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
    // "backend" code for the mobile app
    console.log('PWATime: ', Date.now());
    next();
}, (req, res) => {
    res.render('pwa', { title: 'PWA' });
});

module.exports = router;